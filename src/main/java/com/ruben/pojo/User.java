package com.ruben.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @ClassName: User
 * @Description:
 * @Date: 2020/8/6 20:22
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @NotBlank(message = "用户名不能为空", groups = {UserCheck.class})
    private String username;
    @NotBlank(message = "密码不能为空", groups = UserCheck.class)
    private String password;

    public interface UserCheck {

    }
}
