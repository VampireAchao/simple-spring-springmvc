package com.ruben.resolver;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: GlobalExceptionResolver
 * @Description:
 * @Date: 2020/8/17 20:03
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
public class GlobalExceptionResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        e.printStackTrace();
        ModelAndView modelAndView = new ModelAndView();
        FastJsonJsonView fastJsonJsonView = new FastJsonJsonView();
        Map<String, Object> map = new HashMap<>(1 << 2);
        map.put("success", false);
        map.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
        map.put("msg", "内部错误");
        fastJsonJsonView.setAttributesMap(map);
        modelAndView.setView(fastJsonJsonView);
        return modelAndView;
    }
}
