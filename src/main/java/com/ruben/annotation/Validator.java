package com.ruben.annotation;

import java.lang.annotation.*;

/**
 * @ClassName: Validator
 * @Description:
 * @Date: 2020/8/17 20:57
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Validator {
    String value() default "bindingResult";
}
