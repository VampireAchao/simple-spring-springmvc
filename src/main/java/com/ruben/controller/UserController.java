package com.ruben.controller;

import com.ruben.annotation.Validator;
import com.ruben.pojo.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: UserController
 * @Description:
 * @Date: 2020/8/6 19:56
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 登录失败时调用该接口
     *
     * @return 返回json格式的map
     */
    @GetMapping("shout")
    public Map<String, Object> shout() {
        Map<String, Object> map = new HashMap<>(1 << 2);
        map.put("success", true);
        map.put("code", 200);
        map.put("msg", "哼哼啊啊啊啊啊啊啊啊啊啊");
        return map;
    }

    /**
     * 登录成功时调用该接口，传入一句话
     *
     * @param word 参数 例子：?word=登录成功！
     * @return 返回json格式的map
     */
    @GetMapping("say")
    public Map<String, Object> say(@RequestParam String word) {
        Map<String, Object> map = new HashMap<>(1 << 3);
        map.put("success", true);
        map.put("code", 200);
        map.put("msg", "操作成功");
        map.put("data", word);
        return map;
    }

    /**
     * 登录
     *
     * @param user 参数为Json格式的User对象{"username":"","password":""}
     * @return 返回json格式的map
     */
    @Validator
    @PostMapping("login")
    public Map<String, Object> login(@Validated({User.UserCheck.class})
                                     @RequestBody User user,
                                     BindingResult bindingResult) {
        Map<String, Object> map = new HashMap<>(1 << 2);
        String myUsername = "achao";
        String myPassword = "ruben";
        if (myUsername.equals(user.getUsername()) && myPassword.equals(user.getPassword())) {
            map.put("success", true);
            map.put("code", 200);
            map.put("msg", "登录成功！");
        } else {
            map.put("success", false);
            map.put("code", -629);
            map.put("msg", "登录失败，用户名密码错误！");
        }
        return map;
    }

}
