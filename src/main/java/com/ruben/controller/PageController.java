package com.ruben.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @ClassName: PageController
 * @Description: 页面跳转Controller,前后端分离项目不需要
 * @Date: 2020/8/6 19:54
 * *
 * @author: achao<achao1441470436 @ gmail.com>
 * @version: 1.0
 * @since: JDK 1.8
 */
@Controller
public class PageController {

    /**
     * 页面跳转接口
     *
     * @param page 页面（如index）
     * @return 页面(被视图解析器加上前缀和后缀，如“/WEB-INF/index.jsp”)
     */
    @GetMapping("{page}")
    public String dispatcher(@PathVariable("page") String page) {
        return page;
    }
}
