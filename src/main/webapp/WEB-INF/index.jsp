<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2020/8/6
  Time: 20:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ruben快乐阿超</title>
    <link rel="stylesheet" href="/static/css/index.css"/>
    <script type="application/javascript" src="/static/js/index.js"></script>
</head>
<body>
<div class="box" id="loginForm">
    <h1>登录</h1>
    <input type="text" name="username" placeholder="用户名" value="achao">
    <input type="password" name="password" placeholder="密码" value="ruben">
    <input type="submit" placeholder="登录" onclick="submit()">
</div>
<script type="application/javascript">
    function submit() {
        var form = document.getElementById("loginForm");
        var elements = [];
        var tagElements = form.getElementsByTagName('input');
        for (var j = 0; j < tagElements.length; j++) {
            elements.push(tagElements[j]);
        }
        var user = {"username": "", "password": ""};
        elements.forEach(e => {
            if (e.name === "username") {
                user.username = e.value;
            } else if (e.name === "password") {
                user.password = e.value;
            }
        });
        var response = {
            say: {
                success: function (data) {
                    alert(data.data);
                },
                error: function (data) {

                }
            },
            shout: {
                success: function (data) {
                    alert(data.msg);
                },
                error: function (data) {

                }
            },
            success: function (data) {
                var dataJson = {word: data.msg};
                Ajax.get("/user/say", dataJson, this.say);
            },
            error: function (data) {
                alert(data.msg);
                Ajax.get("/user/shout", null, this.shout);
            }
        }
        Ajax.post("/user/login", JSON.stringify(user), response);
    }
</script>
</body>
</html>
